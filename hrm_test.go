package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"testing"
)

func testFile(t *testing.T, filename string, instructions int, steps int64) {
	// Annoying that this is required
	log.SetFlags(0)
	log.SetOutput(ioutil.Discard)

	runner := NewRunnable(Cells{})
	runner.LoadFile(filename)
	runner.Run()

	// Convert the outbox to CSV for easy comparison
	if len(runner.Result) != len(runner.Outbox) {
		t.Fail()
	}
	for i, v := range runner.Outbox {
		if runner.Result[i] != v {
			fmt.Printf("cell %d: %d / %d\n", i, v.V, runner.Result[i].V)
			t.Fail()
		}
	}
	if steps > 0 && runner.Stepcount != steps {
		fmt.Printf("steps: %d / %d\n", steps, runner.Stepcount)
		t.Fail()
	}
	if instructions > 0 && len(runner.Program.Code) != instructions {
		fmt.Printf("instructions: %d / %d\n", instructions, len(runner.Program.Code))
		t.Fail()
	}
}

func TestAll(t *testing.T) {
	testFile(t, "testcode/sorting.txt", 0, 0)
	testFile(t, "testcode/tetracontiplier-15-60.txt", 15, 60)
	testFile(t, "testcode/tetracontiplier-14-56.txt", 14, 56)
	testFile(t, "testcode/zeropreservation.txt", 7, 25)
	testFile(t, "testcode/zeropreservation-opt.txt", 5, 28)
	testFile(t, "testcode/equalised.txt", 9, 28)
	testFile(t, "testcode/equalised-size.txt", 14, 26)
	testFile(t, "testcode/positivity.txt", 8, 39)
	testFile(t, "testcode/StringStorageFloor.txt", 7, 207)
}
