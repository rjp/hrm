--
-- 24=0
-- 20=idx
-- 23=swaps
-- 22=i
-- 21=i+1

q:
-- set i=0
COPYFROM 24
COPYTO 20
a:
INBOX
COPYTO [20]
JUMPZ z
BUMPUP 20
JUMP a
z:
-- remember our end point
COPYFROM 20
COPYTO 18
b:
-- set j=0, k=1, s=0
COPYFROM 24
-- X
COPYTO 21
COPYTO 22
COPYTO 23
-- X
BUMPUP 21
c:
-- if we're at the end, check for swaps
-- hand=!21 here
SUB 18
JUMPZ h
-- compare N, N+1
COPYFROM [22]
SUB [21]
JUMPN d
JUMPZ d
-- swap [22] and [21]
COPYTO 19
ADD [21]
COPYTO [21]
SUB 19
COPYTO [22]
BUMPUP 23
d:
-- step forwards
BUMPUP 22
BUMPUP 21
JUMP c
h:
-- no swaps means we're finished
COPYFROM 23
JUMPZ e
BUMPDN 18
-- not finished yet, loop again
JUMP b
e:
COPYFROM 24
COPYTO 23
f:
-- output loop
COPYFROM [23]
JUMPZ g
OUTBOX
BUMPUP 23
JUMP f
g:
JUMP q
