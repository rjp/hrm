# A quick interpreter for Human Resource Machine programs

# OPTIONS

* `-code` is a file containing an HRM program (exported via copy or hand-written)
* `-floor` is a comma-separated list of pos:value pairs (24:0,25:1)
* `-inbox` is a comma-separated list of the inbox values

# SOURCE

There are a few meta-instructions that can be used in a source file to set the environment.

* `STACK <csv>` sets the inbox (same as `-inbox`)
* `RESULT <csv>` sets the expected state of the outbox (only useful for testing)
* `CELLS A:B,C:D,E:F...` sets floor cell A to B, cell C to D, etc. (same as `-floor`)
* `FLOOR <csv>` sets floor cells, in order, to the values

# CAVEATS

* The step count isn't 100% aligned with real-HRM because "average computed by testing
your solution against many deterministically generated input sets". But it's close
enough to help with the step-count optimisation challenges.

  e.g. The code in `testcode/positivity.txt` and an inbox of
 `4,-2,8,0,-7,-8,8` will take 41 steps (counted by hand) in both
 real-HRM and this emulator -but- real-HRM will report "Your
 solution completes in 40 steps".

# TESTING

`test.sh` is an example of how to run programs.
`go test` will check the same sort routine as `test.sh` but internally.
