package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type Cell struct {
	V        int64
	HasValue bool
}

type Cells []Cell

func (l Cells) Debug() string {
	out := "[ "
	for _, x := range l {
		out = out + fmt.Sprintf("%3s | ", x.String())
	}
	if len(out) > 2 {
		out = out[0 : len(out)-2]
	}
	return out + "]"
}

// Set the value of a cell + flag
func (c Cell) Set(i int64) {
	c.V = i
	c.HasValue = true
}

func (c Cell) String() string {
	// Empty cell
	if c.HasValue == false {
		return "_"
	}

	// A number
	if c.V < 9000 {
		return fmt.Sprintf("%d", c.V)
	}

	// A character
	return fmt.Sprintf("%s", string(c.V-9000+65))
}

// `NewCell` creates a new cell from a number or character
func NewCell(s string) Cell {
	if s == "" {
		return Cell{V: -1, HasValue: false}
	}

	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		i = int64(s[0]) - 65 + 9000
	}
	return Cell{V: i, HasValue: true}
}

// `NewCellFromInt` creates a new cell explicitly from a number
func NewCellFromInt(i int64) Cell {
	return Cell{V: i, HasValue: true}
}

type Step struct {
	Command  string
	Cell     int64
	Label    string
	Original string
	Indirect bool
}

type Program struct {
	Code   []*Step
	Labels map[string]int64
}

type Item struct {
	Int  *int
	Char *string
}

type Runnable struct {
	Program   *Program
	PC        int64
	Hand      Cell
	Floor     Cells
	Inbox     Cells
	Outbox    Cells
	Result    Cells
	Stepcount int64
}

func main() {
	codefile := flag.String("code", "", "File containing HRM code")
	floor := flag.String("floor", "", "CSV containing floor data")
	inbox := flag.String("inbox", "", "CSV containing inbox data")
	verbose := flag.Bool("verbose", false, "Verbose output or nada")
	flag.Parse()

	fmt.Printf("VERBOSE IS %s\n", strconv.FormatBool(*verbose))

	if !*verbose {
		log.SetFlags(0)
		log.SetOutput(ioutil.Discard)
	}

	log.Printf("Inbox is %s\n", *inbox)

	// Populate the inbox with user-provided data
	f := func(c rune) bool { return !unicode.IsLetter(c) && !unicode.IsNumber(c) && c != rune('-') }
	i := strings.FieldsFunc(*inbox, f)
	inboxData := Cells{}
	for _, x := range i {
		q := NewCell(x)
		q.HasValue = true
		log.Printf("[%s] => [%d] [%s]\n", x, q.V, q)
		inboxData = append(inboxData, q)
	}

	runner := NewRunnable(inboxData)

	for i := 0; i < 25; i++ {
		q := NewCellFromInt(0)
		runner.Floor[i] = q
	}
	runner.LoadFile(*codefile)

    // Allow a command line `-inbox` to override a `STACK` meta
	if len(inboxData) > 0 {
		runner.Inbox = inboxData
	}

	// If we've been given a floor specification, translate that into
	// a collection of cells
	if *floor != "" {
		floorbox := int64(0)
		i := strings.Split(*floor, ",")
		for _, fd := range i {
			parts := strings.Split(fd, ":")
			var q Cell
			if len(parts) > 1 {
				cell, _ := strconv.ParseInt(parts[0], 10, 64)
				q = NewCell(parts[1])
				floorbox = cell
				q.HasValue = true
			} else {
				q = NewCell(parts[0])
				q.HasValue = true
				if parts[0] == "" {
					q.HasValue = false
				}
			}
			runner.Floor[floorbox] = q
			floorbox++
		}
		fmt.Printf("=Floor : %s\n", runner.Floor)
	}
	fmt.Printf("=Inbox : %s\n", runner.Inbox)
	fmt.Println("--BEGIN--")

	runner.Run()

	fmt.Printf("!Inbox : %s\n", runner.Inbox)
	fmt.Printf("!Floor : %s\n", runner.Floor)
	fmt.Printf("!Outbox: %s\n", runner.Outbox)
	if len(runner.Result) > 0 {
		fmt.Printf("!Wanted: %s\n", runner.Outbox)
	}
	fmt.Printf("%d instructions, %d steps\n", len(runner.Program.Code), runner.Stepcount)
}

func NewRunnable(inboxData Cells) *Runnable {
	program := &Program{
		Labels: make(map[string]int64),
	}

	return &Runnable{
		Program: program,
		Inbox:   inboxData,
		Outbox:  Cells{},
		// TODO fix this to by dynamic
		Floor: make(Cells, 25),
	}
}

func (runner *Runnable) Run() {
	runner.Stepcount = 0
	for finished := false; finished != true; {
		pcChanged := false
		step := runner.Step()
		log.Printf("Executing <%s> from pc=%d\n", step.Original, runner.PC)
		if step.Label != "" {
			log.Printf("Target is %s, indirect=%s\n", step.Label, strconv.FormatBool(step.Indirect))
		}
		switch step.Command {
		case "INBOX":
			runner.Hand = runner.INBOX()
			// HRM has a special case of "nothing from INBOX means finished"
			if runner.Hand.HasValue == false {
				log.Println("INBOX empty")
				finished = true
				runner.Stepcount--
				break
			}
		case "OUTBOX":
			runner.OUTBOX()
		case "JUMP":
			pcChanged = runner.JUMP(step.Label)
		case "COPYFROM":
			idx := runner.ResolveCell(step)
			runner.COPYFROM(idx)
		case "COPYTO":
			idx := runner.ResolveCell(step)
			runner.COPYTO(idx)
		case "JUMPZ":
			pcChanged = runner.JUMPZ(step.Label)
		case "JUMPN":
			pcChanged = runner.JUMPN(step.Label)
		case "SUB":
			idx := runner.ResolveCell(step)
			runner.SUB(idx)
		case "ADD":
			idx := runner.ResolveCell(step)
			runner.ADD(idx)
		case "BUMPUP":
			cell, _ := strconv.ParseInt(step.Label, 10, 64)
			runner.BUMPUP(cell)
		case "BUMPDN":
			cell, _ := strconv.ParseInt(step.Label, 10, 64)
			runner.BUMPDN(cell)
		}
		log.Printf("HAND %s\n", runner.Hand)
		log.Printf("PC=%d\n   [%s]\nF: %s\nO: %s\n", runner.PC, floorNums(), runner.Floor.Debug(), runner.Outbox.Debug())

		// If we didn't `JUMP`, move forward to the next instruction.
		if pcChanged == false {
			runner.PC = runner.PC + 1
		}

		// We want to optimise our step count which means we need to remember it.
		runner.Stepcount++
	}
}

// `Step` fetches the current instruction.
func (r *Runnable) Step() *Step {
	return r.Program.Code[r.PC]
}

// `INBOX` fetches a value into our hand.
func (r *Runnable) INBOX() Cell {
	if len(r.Inbox) == 0 {
		return Cell{V: 0, HasValue: false}
	}
	first := r.Inbox[0]
	r.Inbox = r.Inbox[1:]

	first.HasValue = true
	return first
}

// `COPYTO` copies from our hand to a cell.
func (r *Runnable) COPYTO(idx int64) {
	// If we're not holding anything, we can't `COPYTO`, let's `panic`.
	if r.Hand.HasValue == false {
		panic(errors.New("Nothing in hand to COPYTO"))
	}
	r.Floor[idx] = r.Hand
}

// `JUMP` unconditionally jumps to a label.
func (r *Runnable) JUMP(label string) bool {
	r.PC = r.Program.Labels[label]
	return true
}

// `ADD` adds the value in our hand to a value in a cell.
func (r *Runnable) ADD(cell int64) {
	s := r.Floor[cell]
	r.Hand.V = r.Hand.V + s.V
}

func (r *Runnable) SUB(cell int64) {
	s := r.Floor[cell]
	r.Hand.V = r.Hand.V - s.V
}

func (r *Runnable) JUMPZ(label string) bool {
	if r.Hand.V == 0 {
		r.PC = r.Program.Labels[label]
		return true
	}

	return false
}

func (r *Runnable) JUMPN(label string) bool {
	if r.Hand.V < 0 {
		r.PC = r.Program.Labels[label]
		return true
	}

	return false
}

func (r *Runnable) COPYFROM(idx int64) {
	if r.Floor[idx].HasValue == false {
		panic(errors.New("Cannot COPYFROM empty cells"))
	}
	r.Hand = r.Floor[idx]
}

func (r *Runnable) OUTBOX() {
	r.Outbox = append(r.Outbox, r.Hand)
	r.Hand = Cell{V: 0, HasValue: false}
}

func (r *Runnable) BUMPUP(cell int64) {
	if r.Floor[cell].HasValue == false {
		panic(errors.New("Cannot BUMPUP an empty cell"))
	}
	r.Floor[cell].V++
	r.Hand = r.Floor[cell]
}

func (r *Runnable) BUMPDN(cell int64) {
	if r.Floor[cell].HasValue == false {
		panic(errors.New("Cannot BUMPUP an empty cell"))
	}
	r.Floor[cell].V--
	r.Hand = r.Floor[cell]
}

func (r *Runnable) ResolveCell(step *Step) int64 {
	idx, _ := strconv.ParseInt(step.Label, 10, 64)
	if step.Indirect {
		if r.Floor[idx].HasValue == false {
			panic(errors.New("Cannot access an empty cell"))
		}
		idx = r.Floor[idx].V
	}
	return idx
}

// `floorNums` generates the number labels for our debug floor output.
// TODO make this work on the right size of the floor
func floorNums() string {
	a := []string{}
	for i := 0; i < 25; i++ {
		a = append(a, fmt.Sprintf(" %3d ", i))
	}
	return strings.Join(a, "|")
}

func (r *Runnable) LoadFile(filename string) {
	fh, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	r.Load(fh)
}

func (r *Runnable) Load(f io.Reader) {
	p := r.Program

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := string(strings.TrimSpace(scanner.Text()))
		parts := strings.Fields(line)
		if len(parts) > 0 {
			// Ignore the comment at the beginning.
			if parts[0] == "--" {
				continue
			}
			// If we hit the graphical labels for floor boxes or comments, give up.
			if parts[0] == "DEFINE" {
				break
			}
			// Ignore the locations of comments
			if parts[0] == "COMMENT" {
				continue
			}
			// Allow the program file to specify the stack (helps with testing)
			if parts[0] == "STACK" {
				r.Inbox = makeCells(parts[1])
				continue
			}
			if parts[0] == "RESULT" {
				r.Result = makeCells(parts[1])
				continue
			}
			if parts[0] == "CELLS" {
				for _, i := range strings.Split(parts[1], ",") {
					cv := strings.SplitN(i, ":", 2)
					c, _ := strconv.ParseInt(cv[0], 10, 32)
					r.Floor[c] = NewCell(cv[1])
				}
				continue
			}
			if parts[0] == "FLOOR" {
				for i, v := range strings.Split(parts[1], ",") {
					r.Floor[i] = NewCell(v)
				}
				continue
			}
			// Jump labels end in `:`
			if parts[0][len(parts[0])-1] == ':' {
				p.Labels[parts[0][0:len(parts[0])-1]] = int64(len(p.Code))
			} else {
				// Not a jump label.
				label := ""
				indirect := false
				// Does the command have a parameter?
				if len(parts) > 1 {
					label = parts[1]
					// Is the parameter an indirect cell reference?
					if string(label[0]) == "[" {
						indirect = true
						label = label[1 : len(label)-1]
					}
				}
				p.Code = append(p.Code,
					&Step{
						Command: parts[0], Label: label, Original: line, Indirect: indirect,
					})
			}
		}
	}

	// Resolve all the jump references now we know where all the destinations are
	for _, i := range p.Code {
		log.Printf("C=[%s] L=[%s] B=[%s]\n", i.Command, i.Label, strconv.FormatBool(i.Indirect))
		if len(i.Command) > 3 && i.Command[0:4] == "JUMP" {
			log.Printf("Resolving %s to step %d\n", i.Label, p.Labels[i.Label])
			// TODO complain if we don't have this label defined
			i.Cell = p.Labels[i.Label]
		}
	}
}

func makeCells(in string) Cells {
	inbox := Cells{}
	for _, i := range strings.Split(in, ",") {
		q := NewCell(i)
		inbox = append(inbox, q)
	}
	return inbox
}
